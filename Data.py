from faker import Faker


class FakeData():
    """Class to create mock data."""

    def __init__(self) -> None:
        self.fake = Faker()

    def get_registered_user(self):
        """Create mock data."""
        return {
            "name": self.fake.name(),
            "address": self.fake.address(),
            "created_at": self.fake.year()
        }
