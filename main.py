import time
from KafkaManager import KafkaManager
from Data import FakeData
from config_manager import get_consumer_group_id, get_topic, get_action

if __name__ == "__main__":
    action = get_action()

    if action == 'produce':
        while 1==1:
            registered_user = FakeData().get_registered_user()
            KafkaManager().send_data_to_kafka_topic(
                                            data_to_send=registered_user,
                                            topic=get_topic("topic-reg-user"),
                                            part=None)
            time.sleep(2)

    elif action == 'consume':
        KafkaManager().consume_data_from_kafka_topic(
                                        topic=get_topic("topic-reg-user"),
                                        c_group_id=get_consumer_group_id("a"))
