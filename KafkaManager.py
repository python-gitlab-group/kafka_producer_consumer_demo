from kafka import KafkaProducer, KafkaConsumer
from config_manager import get_config
import json


class KafkaManager():
    """Class to produce data and send to kafka broker (server)."""

    def __init__(self) -> None:
        self.config = get_config()
        self.kafka_broker = self.config['kafka-config']['server']
        self.kafka_port = self.config['kafka-config']['port']


    @staticmethod
    def json_serializer(data):
        return json.dumps(data).encode("utf-8")

    def send_data_to_kafka_topic(self, data_to_send: dict, topic: str,
                                 part: int=None):
        """Send data to Kafka topic/partition.
            The number of partitions and Brokers for that specific topic must
            be managed on Kafka Server Configuration.

        Args:
            data_to_send (dict): Data dictionary to send.
            topic (str): Name of topic to send data to.
            part(int): Partition to send data to. None means the function sends
            data randomly to the available partitions.
        """

        producer = KafkaProducer(
            bootstrap_servers=[f"{self.kafka_broker}:{self.kafka_port}"],
            value_serializer=self.json_serializer)

        print(f"Sending to {topic} topic: {data_to_send}")
        producer.send(topic, data_to_send, partition=part)

    def consume_data_from_kafka_topic(self, topic: str, c_group_id: str):
        """Consume data from the specified topic.

        Args:
            topic (str): Name of topic to read data from.
            group_id (str): Consumer Group ID.
        """
        consumer = KafkaConsumer(
            topic,
            bootstrap_servers=[f"{self.kafka_broker}:{self.kafka_port}"],
            auto_offset_reset='earliest',
            group_id=c_group_id
        )

        print(f"Consuming data from topic {topic}, consumer group id {c_group_id}")
        for msg in consumer:
            print(f"{topic}: {json.loads(msg.value)}")
