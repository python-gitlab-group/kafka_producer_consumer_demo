import json
from typing import KeysView

def get_config():

    # Opening JSON file
    f = open('config.json',)

    # returns JSON object as a dictionary
    data = json.load(f)

    # Closing file
    f.close()

    return data

def get_topic(topic: str) -> str:
    """Get kafka topic name.

    Args:
        topic (str): Json config key.

    Returns:
        str: Topic name.
    """
    data = get_config()

    for i in data['kafka-config']['topics']:

        if i.get(topic) != None:
            return i[topic]

    return ""

def get_action() -> str:
    """Get action to run.

        produce: Generate data and send it to Kafka topic.
        consume: Consume data from a Kafka topic.
    """
    data = get_config()

    if data.get('action') != None:
        return data.get('action')

    return ""

def get_consumer_group_id(group: str) -> str:
    """Get kafka consumer group id.

    Args:
        group (str): Json config key.

    Returns:
        str: Consumer Group Id.
    """
    data = get_config()

    for i in data['kafka-config']['consumer-groups']:

        if i.get(group) != None:
            return i[group]

    return ""
